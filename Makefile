SRC=solution/src
C=solution/src/c
SSE=solution/src/sse

all: build c_build sse_build

c_build:
	clang -o build/sepia_c $(SRC)/*.c $(C)/*.c

sse_build: sepia.o
	clang -o build/sepia_sse $(SRC)/*.c $(SSE)/*.c sepia.o

build:
	mkdir -p build

sepia.o: $(SRC)/sepia_magic.asm
	nasm -felf64 -g -o sepia.o $(SRC)/sepia_magic.asm

clean:
	rm -rf build sepia.o


