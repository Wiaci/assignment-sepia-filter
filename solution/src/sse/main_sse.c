#include "../../include/bmp_transform_sse.h"

#include <sys/resource.h>
#include <stdio.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // suppress 'unused parameters' warning

    struct rusage r;
    struct timeval start;
    struct timeval end;
    long sum = 0;
    for (int i = 0; i < 100; i++) {
        getrusage(RUSAGE_SELF, &r);
        start = r.ru_utime;
        sepia(argv[1], argv[2]);
        getrusage(RUSAGE_SELF, &r);
        end = r.ru_utime;
        long res = ((end.tv_sec - start.tv_sec) * 1000000L) +
                   end.tv_usec - start.tv_usec;
        printf("Time elapsed in microseconds: %ld\n", res);
        sum += res;
    }
    printf("Average: %ld\n", sum / 100);

    return 0;
}
