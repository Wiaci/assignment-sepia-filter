#include <stdint.h>
#include <stdio.h>

#include "../include/bmp.h"

#define BMP_SIGNATURE 19778
#define BMP_BIT_COUNT 24
#define BMP_HEADER_SIZE 54

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static uint64_t padding(uint64_t width) {
    uint64_t remainder = (width * 3) % 4;
    if (remainder == 0) return 0;
    return 4 - remainder;
}

enum read_status fill_image(FILE* in, struct image* img) {
    uint64_t width = img->width;
    uint64_t height = img->height;
    for (size_t i = 0; i < height; i++) {
        size_t pixels_read = fread(img->data + (width * i),sizeof(struct pixel), width, in);
        if (pixels_read != width) return READ_TOO_SMALL_DATA_SIZE;
        uint64_t current_padding = 0;
        size_t pad_read = fread(&current_padding, 1, padding(width), in);
        if (pad_read != padding(width)) return READ_TOO_SMALL_DATA_SIZE;
    }
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    size_t header_read = fread(&header, sizeof(struct bmp_header), 1, in);
    uint16_t bfType = header.bfType;
    uint64_t width = header.biWidth;
    uint64_t height = header.biHeight;
    uint16_t biBitCount = header.biBitCount;
    if (header_read != 1) return READ_INVALID_HEADER;
    if (bfType != BMP_SIGNATURE) return READ_INVALID_SIGNATURE;
    if (biBitCount != BMP_BIT_COUNT) return READ_INVALID_BITS;
    *img = image_create(width, height);
    if (fill_image(in, img) == READ_TOO_SMALL_DATA_SIZE) {
        return READ_TOO_SMALL_DATA_SIZE;
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    uint32_t width = img->width;
    uint32_t height = img->height;
    uint32_t pad = padding(width);
    uint32_t image_size = (width * 3 + pad) * height;
    struct bmp_header header = (struct bmp_header) {
            .bfType = BMP_SIGNATURE,
            .bfileSize = BMP_HEADER_SIZE + image_size,
            .bfReserved = 0,
            .bOffBits = BMP_HEADER_SIZE,
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    fwrite(&header, sizeof(struct bmp_header), 1, out);
    for (size_t i = 0; i < height; i = i + 1) {
        size_t pixels_written = fwrite(&img->data[width * i], sizeof(struct pixel), width, out);
        if (pixels_written != width) return WRITE_ERROR;
        uint8_t zero = 0;
        size_t pad_written = fwrite(&zero, 1, pad, out);
        if (pad_written != pad) return WRITE_ERROR;
    }
    return WRITE_OK;
}
