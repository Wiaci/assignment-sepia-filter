#include "../include/files.h"

FILE* file_open_read(const char* filename) {
    return fopen(filename, "r");
}

FILE* file_open_write(const char* filename) {
    return fopen(filename, "w");
}

bool close_file(FILE* f) {
    if (fclose(f) != 0) return false;
    return true;
}
