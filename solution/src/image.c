#include "../include/image.h"
#include "stdint.h"
#include <malloc.h>

struct image image_create(uint64_t width, uint64_t height) {
    struct image img = (struct image) {
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * height * width)
    };
    return img;
}

void image_destroy(struct image *img) {
    if (!img) return;
    if (img->data) {
        free(img->data);
    }
}

struct pixel* pixel_of(struct image img, uint32_t x, uint32_t y) {
    return &img.data[img.width * x + y];
}
