section .rodata
align 16
c1: dd 0.131, 0.168, 0.189, 0.131
align 16
c2: dd 0.543, 0.686, 0.769, 0.543
align 16
c3: dd 0.272, 0.349, 0.393, 0.272
align 16
max: dd 255.0, 255.0, 255.0, 255.0

section .text

global sse
; rdi - src
; rsi - dest
sse:

    movdqa xmm3, [c1]
    movdqa xmm4, [c2]
    movdqa xmm5, [c3]

    movdqu xmm0, [rdi]
    shufps xmm0, xmm0, 11000000b
   
    add rdi, 4
    movdqu xmm1, [rdi]
    shufps xmm1, xmm1, 11000000b

    add rdi, 4
    movdqu xmm2, [rdi]
    shufps xmm2, xmm2, 11000000b

    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5

    addps xmm0, xmm1
    addps xmm0, xmm2

    pminsd xmm0, [max]
    cvtps2dq xmm0, xmm0
    pextrb [rsi], xmm0, 0
    pextrb [rsi+1], xmm0, 4
    pextrb [rsi+2], xmm0, 8
    pextrb [rsi+3], xmm0, 12

    add rsi, 4
    add rdi, 4
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    shufps xmm3, xmm3, 01111001b
    shufps xmm4, xmm4, 01111001b
    shufps xmm5, xmm5, 01111001b

    movdqu xmm0, [rdi]
    shufps xmm0, xmm0, 11110000b
   
    add rdi, 4
    movdqu xmm1, [rdi]
    shufps xmm1, xmm1, 11110000b

    add rdi, 4
    movdqu xmm2, [rdi]
    shufps xmm2, xmm2, 11110000b

    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5

    addps xmm0, xmm1
    addps xmm0, xmm2

    pminsd xmm0, [max]
    cvtps2dq xmm0, xmm0
    pextrb [rsi], xmm0, 0
    pextrb [rsi+1], xmm0, 4
    pextrb [rsi+2], xmm0, 8
    pextrb [rsi+3], xmm0, 12

    add rsi, 4
    add rdi, 4
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    shufps xmm3, xmm3, 01111001b
    shufps xmm4, xmm4, 01111001b
    shufps xmm5, xmm5, 01111001b

    movdqu xmm0, [rdi]
    shufps xmm0, xmm0, 11111100b
   
    add rdi, 4
    movdqu xmm1, [rdi]
    shufps xmm1, xmm1, 11111100b

    add rdi, 4
    movdqu xmm2, [rdi]
    shufps xmm2, xmm2, 11111100b

    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5

    addps xmm0, xmm1
    addps xmm0, xmm2

    pminsd xmm0, [max]
    cvtps2dq xmm0, xmm0
    pextrb [rsi], xmm0, 0
    pextrb [rsi+1], xmm0, 4
    pextrb [rsi+2], xmm0, 8
    pextrb [rsi+3], xmm0, 12

    ret