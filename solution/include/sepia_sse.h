#ifndef SRC_SEPIA_SSE_H
#define SRC_SEPIA_SSE_H

#include "image.h"

void sepia_c_inplace( struct image* img );
void sepia_sse_inplace( struct image* img );

#endif //SRC_SEPIA_SSE_H
