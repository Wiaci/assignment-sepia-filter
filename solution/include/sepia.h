#ifndef ASSIGNMENT_SEPIA_FILTER_SEPIA_H
#define ASSIGNMENT_SEPIA_FILTER_SEPIA_H

#include "image.h"

void sepia_c_inplace( struct image* img );
void sepia_sse_inplace( struct image* img );

#endif //ASSIGNMENT_SEPIA_FILTER_SEPIA_H
