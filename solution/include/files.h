#ifndef ASSIGNMENT_IMAGE_ROTATION_FILES_H
#define ASSIGNMENT_IMAGE_ROTATION_FILES_H

#include <stdbool.h>
#include <stdio.h>

bool close_file(FILE* f);
FILE* file_open_read(const char* filename);
FILE* file_open_write(const char* filename);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILES_H
