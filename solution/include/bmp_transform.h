#ifndef SRC_BMP_TRANSFORM_H
#define SRC_BMP_TRANSFORM_H

#include <stdbool.h>

bool sepia(const char *src_filename, const char *dest_filename);

#endif //SRC_BMP_TRANSFORM_H
