#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>

struct __attribute__((packed)) image {
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image image_create(uint64_t width, uint64_t height);
void image_destroy(struct image *img);
struct pixel* pixel_of(struct image img, uint32_t x, uint32_t y);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
